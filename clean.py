#!/usr/bin/env python3

filename = "deltagere-2018.csv"
delimiter = ";"

output_filename = "people.txt"

current_year = 2018

guess_gender = True
print_table = True

import re
import math

from helpers import *



if guess_gender:
    from genderize import Genderize


def process_study_and_year(input_string):
    def check_year(n):
        s = str(n)
        return not "kand" in input_string and (s in input_string or s[2:] in input_string)

    def check_num(n):
        s = str(n)
        return s in input_string

    if not input_string:
        return (Study.Unspecified, Year.Unspecified)

    study = Study.Undetermined
    year  = Year.Undetermined

    if "øk" in input_string:
        study = Study.MatOk
    elif "applied" in input_string:
        study = Study.Anv
    elif "bio" in input_string:
        study = Study.Bio
    elif "kemi" in input_string:
        study = Study.Kemi
    elif "fysik" in input_string:
        study = Study.Fysik
    elif "data" in input_string:
        study = Study.Dat
    elif "anv" in input_string:
        study = Study.Anv
    elif "mat" in input_string:
        study = Study.Mat
    else:
        study = Study.Unspecified

    # Assumption: study has now been determined or is left unspecified

    for i in range(current_year-8, current_year+1):
        if check_year(i):
            year = i
            break

    if study is Study.Bio:
        year = current_year - 4

    # Filter out matches
    if study != Study.Undetermined and year != Year.Undetermined:
        return (study, year)

    for i in range(1,12):
        if check_num(i):
            if "år" in input_string:
                year = current_year - i
                break
            elif "sem" in input_string:
                year = current_year - int(math.ceil(i/2))
                break
    
    if "andet år" in input_string:
        year = current_year - 2

    if year != Year.Undetermined and "kand" in input_string:
        year -= 3

    # Filter out matches
    if year != Year.Undetermined:
        return (study, year)

    if "msc" in input_string or "kand" in input_string or "bio" in input_string:
        year = current_year - 4

    # Filter out matches
    if year != Year.Undetermined:
        return (study, year)

    # Handle special cases
    if "6" == input_string:
        year = 2015

    # Accept unspecified study or yeal
    if year == Year.Undetermined:
        year = year.Unspecified

    return (study, year)


undetermined_people = []
people = []

with open(filename, "r") as f:
    f.readline()
    for raw_line in f:
        line = raw_line.strip()
        split = line.split(delimiter)

        gender = Gender.U

        if guess_gender:
            first_name_split = split[0].split()
            first_first_name = first_name_split[0]
            if first_first_name == "Floor":
                gender = Gender.M
            else:
                gender_data = Genderize().get([first_first_name])[0]
                print(gender_data)
                if gender_data["gender"] == "male":
                    gender = Gender.M
                elif gender_data["gender"] == "female":
                    gender = Gender.K


        full_name = split[0] + " " + split[1]
        full_name = re.sub(r"\s\s+", " ", full_name).strip().title()

        email = split[2]

        study_and_year = split[3].strip().lower()
        study, year = process_study_and_year(study_and_year)

        if study == Study.Undetermined or year == Year.Undetermined:
            undetermined_people.append((full_name, study_and_year))
            continue

        p = Person(full_name, study, year, email=email, gender=gender)
        people.append(p)


if len(undetermined_people) > 0:
    for name, study_and_year in undetermined_people:
        print("{} \t | \t {}".format(name, study_and_year))
    exit()

if print_table:
    from tabulate import tabulate # Requires pip packages

    people_table = [[p.name, p.email, p.study_pretty(), p.year_pretty()] for p in people]

    tab = tabulate(people_table, headers=["Navn", "Email", "Studie", "Start år"], tablefmt="fancy_grid")
    print(tab)


with open(output_filename, "w") as f:
    for p in people:
        f.write("{} | {} | {} | {}\n".format(p.name, p.study_pretty(""), p.year_pretty(""), p.gender_pretty("")))
